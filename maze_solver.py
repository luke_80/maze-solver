#!/usr/bin/python
# -*- coding: UTF-8 -*-
"""
maze solver
""" 

import random
import argparse

###########################
#### MAZE SOLVER       ####
###########################

class Maze(object):
    """
    Maze object class
    """

    STAR = '\033[1;36m*\033[1;m'
    BORDER = '#'
    BRICK = '\033[1;30m#\033[1;m'
    START = '\033[1;43mS\033[1;m'
    END = '\033[1;42mE\033[1;m'    

    def __init__(self, width, height, entrance=None, exit=None, *args, **kwargs):
        self.width = width
        self.height = height
        self.entrance = entrance or (0,1)
        self.steps = 0
        if not isinstance(self.entrance, tuple):
            raise ValueError('entrance value must be a tuple')
        self.exit = exit or (self.height-1,self.width-2)
        if not isinstance(self.entrance, tuple):
            raise ValueError('exit value must be a tuple')
        self.maze = self._generate_maze()

    def __repr__(self):
        return "\r\n".join([''.join(m) for m in self.maze])

    def _generate_maze(self):
        maze = [[' ' for x in range(self.width)] for y in range(self.height)]
        maze[0] = [self.BORDER for i in range(self.width)]
        maze[-1] = [self.BORDER for i in range(self.width)]
        for y in range(self.height):
            maze[y][0] = self.BORDER
            maze[y][-1] = self.BORDER
        for y in range(1, self.height-1):
            for x in range(1, self.width-1):
                maze[y][x] = random.choice([self.BRICK,' '])if x%2==0 else ' '
        maze[self.entrance[0]][self.entrance[1]] = self.START
        maze[self.exit[0]][self.exit[1]] = self.END
        return maze

    def solve(self, x=None, y=None, sizex=None, sizey=None):
        found = False
        sizex = sizex or self.width
        sizey = sizey or self.height
        if x is None and y is None:
            y, x = self.entrance
        if 0 <= x < sizex and 0 <= y < sizey:
            if self.maze[y][x] in (' ', self.START):
                if self.maze[y][x] == ' ':
                    self.maze[y][x] = '.' # mark as visited
                if (self.solve(x+1, y, sizex, sizey) or self.solve(x-1, y, sizex, sizey) or
                    self.solve(x, y+1, sizex, sizey) or self.solve(x, y-1, sizex, sizey)):
                    if self.maze[y][x] == '.':
                        self.maze[y][x] = self.STAR
                        self.steps += 1
                    found = True
            elif self.maze[y][x] == self.END:
                found = True
        return found

    def clean_path(self):
        for y in range(self.height):
            self.maze[y] = [x.replace('.', ' ') for x in self.maze[y]]


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Maze generator/solver')
    parser.add_argument("-c","--cols", help="maze width (columns) (default=5)", type=int, default=5)
    parser.add_argument("-r","--rows", help="maze height (rows) (default=5)", type=int, default=5)
    args = parser.parse_args()
    maze_instance = Maze(width=args.cols, height=args.rows)
    print("\nGENERATED MAZE:")
    print(maze_instance)
    try:
        if maze_instance.solve():
            print("\n\nPATH FOUND: (%d steps)" % maze_instance.steps)
            maze_instance.clean_path()
            print(maze_instance)
        else:
            print("\nUNABLE TO FIND PATH")
    except (RuntimeError, TypeError) as e:
        print "SOMETHING WENT WRONG: %s" % e.message

